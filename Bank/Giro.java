class Giro extends Konto {
    protected float kredit;

    public Giro(String inhaber, String iban, String bic, float kredit) {
        super(inhaber, iban, bic);
        this.kredit = kredit;
    }

    public Giro() {
    }

    public boolean auszahlung(float betrag) {
        if (betrag >= 0 && (saldo + kredit) >= betrag) {
            this.saldo -= betrag;
            return true;
        } else {
            return false;
        }

    }

    public boolean ueberweisung(float betrag, Konto target) {
        if (betrag >= 0 && (saldo + kredit) >= betrag) {
            this.saldo -= betrag;
            target.einzahlen(betrag); 
            return true;
        } else {
            return false;
        }
    }

}