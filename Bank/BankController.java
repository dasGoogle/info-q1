class BankController{
    public static void main(String args[]){
        Giro account = new Giro("Aaron Schlitt", "DE79500112345734", "HELADEF1KAS", 1000); 
        Festgeld festgeld = new Festgeld("Henricus Bracht", "DE67877324897849", "HELADEF1KAS", 0.0005f, 365); 
        account.einzahlen(100); 
        account.auszahlung(50); 
        account.ueberweisung(50, festgeld); 
        out("Das Giro hat jetzt einen Kontostand von " + account.getSaldo() + " Euro."); 

        out("Das Festgeld hat jetzt einen Kontostand von " + festgeld.getSaldo() + " Euro."); 

    }
    static void out(String output){
        System.out.println(output); 
    }
    static void outnl(String output){
        System.out.print(output); 
    }
    
}