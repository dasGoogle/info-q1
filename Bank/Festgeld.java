class Festgeld extends Konto{
    protected float zinssatz; 
    protected int laufzeit; 
    public Festgeld(String inhaber, String iban, String bic, float zinssatz, int laufzeit){
        super(inhaber, iban, bic); 
        this.zinssatz = zinssatz; 
    }
    public boolean ueberweisen(Konto target){
        target.einzahlen(slado * (zinssatz+1)*tage/365f); 
        this.saldo = 0; 
        return true; 
    }
}