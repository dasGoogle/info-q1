class Konto{
    protected float saldo; 
    protected String inhaber; 
    protected String iban; 
    protected String bic; 
    protected String bank; 
    public Konto(){}
    public Konto(String inhaber, String iban, String bic){
        this.iban = iban; 
        this.bic = bic; 
        this.inhaber = inhaber; 
    }
    public boolean einzahlen(float betrag){
        if (betrag >= 0){
            saldo += betrag; 
            return true; 
        } else {
            return false; 
        }
    }
    public float getSaldo(){
        return saldo; 
    }
    public String getInhaber(){
        return inhaber; 
    }

}