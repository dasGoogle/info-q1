public class BMI {
    private String name;
    private int size; 
    private float mass;  
    private int gender; 

    public BMI(String name, int size, float mass, int gender){
        this.name = name; 
        this.size = size; 
        this.mass = mass; 
        this.gender = gender; 
    }
    public BMI(){
    }

    public int getSize(){
        return this.size; 
    }
    public void setSize(int size){
        this.size = size; 
    }

    public float getMass(){
        return this.mass;
    }
    public void setMass(float mass){
        this.mass = mass; 
    }

    public float calcBMI(){
        return mass/((size/100f)*(size/100f));
    }

    public String getName(){
        return this.name; 
    }

    public void setName(String name){
        this.name = name;
    }

    public void setGender(int gender){
        this.gender = gender; 
    }

    public int getGender(){
        return this.gender;
    }

    public String genComment(){
        float bmi = calcBMI(); 
        int male[] = {20, 25, 30, 40};
        int female[] = {19, 24, 30, 40};
        int treshold[]; 
        if (gender ==  1) {
            treshold = male; 
        } else {
            treshold = female; 
        } 
        if (bmi < treshold[0]){
            return "Du solltest mehr essen! "; 
        } else if (bmi < treshold[1]) {
            return "Alles Okay! ";
        } else if (bmi < treshold[2]){
            return "Du solltest etwas weniger essen. ";
        } else if (bmi < treshold[3]){
            return "Du solltest einen Arzt aufsuchen. ";
        } else {
            return "Irgendetwas stimmt hier nicht. Du musst dringend zum Arzt. ";
        }
    }

}