

import java.util.Scanner; 

public class BMIcalc {
  
  public static void main(String[] args) {
    Scanner terminal = new Scanner(System.in);
    BMI human = new BMI(); 
    
    System.out.println("Gib Deinen Namen ein: ");
    human.setName(terminal.next());
    System.out.println("Gib Deine K�rpergr��e ein (cm): ");
    human.setSize(terminal.nextInt());
    System.out.println("Gib Dein Gewicht ein (kg): ");
    human.setMass(terminal.nextFloat());
    System.out.println("Gib Dein Geschlecht ein: (1:m, 2:f, 3:a) ");
    human.setGender(terminal.nextInt());
    System.out.println("Dein BMI betr�gt "+ human.calcBMI()); 
    System.out.println(human.genComment());
    terminal.close(); 
  } // end of main

} // end of class Main

