import java.util.Scanner;

class CarController {
    public static void main(String args[]) {
        Scanner terminal = new Scanner(System.in);
        Car garage[] = {new Car("KS A 1234", 7, 100, 30, 50000), new Car("KS B 4567", 7, 100, 10, 50000)}; 
        Car car; 
        int action = 0; 
        int carChooser = 0; 
        
        do {
            System.out.println("Welches Deiner Autos m�chtest Du fahren? (Beenden mit 1000)"); 
            carChooser = terminal.nextInt() -1;
            if (carChooser <= 1){
                car = garage[carChooser]; 
                System.out.println("Dein Auto hat das Kennzeichen " + car.getPlate());
                do {
                    System.out.println("Was m�chtest Du tun? (1-fahren, 2-tanken, 3-Infos, 4-parken)");
                    action = terminal.nextInt();
                    switch (action) {
                    case 1:
                        System.out.println("Du k�nntest noch " + car.calcRestDistance() + " km fahren. ");
                        System.out.print("Wie viele km sollen gefahren werden? ");
                        float distance = terminal.nextFloat();
                        int callback = car.drive(distance); 
                        if (callback!=1) {
                            System.out.println("Okay, " + distance + " km gefahren. ");
                            if(callback == 3){
                                System.out.println("Du musst zur Inspektion. "); 
                            }
                        } else {
                            System.out.println("Das geht nicht, Dein Tank reicht nicht daf�r. Du kannst nur noch " + car.calcRestDistance() + " km fahren. ");
                        }
                        break;
                    case 2:
                        System.out.println("Du kannst bis zu " + car.tankSpace() + " Liter tanken. ");
                        System.out.print("Wie viel Liter m�chtest Du tanken? ");
                        float liters = terminal.nextFloat();
                        if (car.fill(liters)) {
                            System.out.println("Okay, " + liters + " Liter getankt. ");
                        } else {
                            System.out.println("Das geht nicht, Dein Tank l�uft �ber. Es passen nur noch " + car.tankSpace() + " Liter in den Tank. ");
                        }
                        break;
                    case 3: 
                        System.out.println(carInfo(car));
                        break; 
                    }
                } while (action != 4); 
            }
        } while (carChooser != 999); 
        
        terminal.close();
    }

    static String carInfo(Car car) {
        String result = "";
        result += "*---------------------------------------------------------------*\n"; 
        result += "|\tInfos zum Auto mit dem Kennzeichen " + car.getPlate() + "\t\t|";
        result += "\n";
        result += "| Tankkapazit�t\t" + car.getTankCapacity() + " l\t\t|\tF�llung\t" + car.getTankState() + " l\t\t|\n";
        result += "| Tachostand\t" + car.getDistance() + " km\t|\tVerbr.\t" + car.geLitersPerHundred() + " l/100km\t|\n";
        result += "*---------------------------------------------------------------*\n"; 
        return result; 
    }

}