class Car{
  // Anfang Attribute
    private float tankCapacity; 
    private float tankState; 
    private float litersPerHundred; 
    private float distance; 
    private float lastInspection; 
    private String plate; 
  // Ende Attribute

    public Car(String plate, float lph, float tankCapacity, float tankState, float distance){
        this.distance = distance; 
        this.tankState = tankState; 
        this.tankCapacity = tankCapacity; 
        this.litersPerHundred = lph; 
        this.plate = plate; 
        this.lastInspection = distance; 
    }
  // Anfang Methoden

    public float getTankState(){
        return this.tankState; 
    }

    public float getTankCapacity(){
        return this.tankCapacity; 
    }

    public String getPlate(){
        return this.plate; 
    }

    public boolean fill(float liters){
        
        if (tankSpace() >= liters){
            this.tankState += liters; 
            return true; 
        } else {
            return false; 
        }
    }

  public float tankSpace() {
        return 0; 
  }

    public float getDistance(){
        return this.distance; 
    }

    public float calcRestDistance(){
        return tankState/litersPerHundred*100; 
    }

    public float geLitersPerHundred(){
        return litersPerHundred; 
    }

    public int drive(float distance){
        float fuelConsumed = distance/100 * this.litersPerHundred; 
        if (fuelConsumed <= this.tankState){
            this.distance += distance; 
            this.tankState -= fuelConsumed; 
            if (this.distance - lastInspection > 20000){
                lastInspection = this.distance; 
                return 3; 
            }
            return 0; 
        } else {
            return 1; 
        }
    }
  // Ende Methoden



}
