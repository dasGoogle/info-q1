import java.util.Arrays;

class JavaSort {
    public static void main(String args[]) {
        

        int arrayLength = 1000000; 
        int array[] = new int[arrayLength];
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < arrayLength; i++) {
            array[i] = (int) (Math.random() * arrayLength) + 1;
        }

        Arrays.sort(array);

        long endTime = System.currentTimeMillis();

        System.out.println("------------");

        System.out.println((endTime - startTime) / 1000f);
    }
}