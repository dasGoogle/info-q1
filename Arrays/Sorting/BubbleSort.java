class BubbleSort {
public static void main(String args[]){
    int arrayLength = 1000000; 
    int array[] = new int[arrayLength]; 
    long startTime = System.currentTimeMillis(); 

    for (int i=0; i<arrayLength; i++){
        array[i]=(int) (Math.random()*arrayLength)+1; 
    }

    

    for (int i = arrayLength-2; i >= 0; i--){
        for (int j = 0; j <= i; j++){
            if(array[j] > array[j+1]){
                int buffer = array[j]; 
                array[j] = array[j+1]; 
                array[j+1] = buffer; 
            }
        }
    }

    long endTime = System.currentTimeMillis(); 

    System.out.println((endTime - startTime)/1000f); 

    
}

}
