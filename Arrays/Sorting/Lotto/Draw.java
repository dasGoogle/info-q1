import java.util.*; 
class Draw{
    int result[]; 
    int s; 
    public Draw(){
        result = new int[6]; 
        for(int i=0; i < 6; i++){
            int cran; 
            do {
                cran = genRan(1,49);
            } while (isElement(result, cran));
            result[i]=cran;
        }
        s=genRan(0,9); 
        Arrays.sort(result); 
    }

    public int[] getResult(){
        return result; 
    }

    public int getSuper(){
        return s; 
    }

    private int genRan(int l, int h){
        return (int) (Math.random()*(h-l)+1)+l; 
    }

    public static boolean isElement(int array[], int element){
        for(int i=0; i< array.length; i++){
            if(array[i]==element){
                return true;
            }
        }
        return false; 
    }
}