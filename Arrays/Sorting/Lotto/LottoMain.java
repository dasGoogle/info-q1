import java.io.*;
import java.util.Scanner;

class LottoMain {
    public static void main(String args[]) {
        Scanner term = new Scanner(System.in);
        int input[] = new int[6];
        int superzahl;
        System.out.println("\tLotto spielen");
        System.out.println("--------------------------------------");
        for (int i = 0; i < 6; i++) {
            int in;
            do {
                System.out.print("Gib Deine " + (i + 1) + ". Zahl ein:");
                in = term.nextInt();
            } while (in > 49 || in < 1 || Draw.isElement(input, in));
            input[i] = in;
        }
        System.out.println("\n");
        do {
            System.out.print("Gib nun Deine Superzahl ein: ");
            superzahl = term.nextInt();
        } while (superzahl < 0 || superzahl > 9);

        Ticket ticket = new Ticket(input, superzahl);

        Draw draw = new Draw();

        System.out.println("Die Gewinnzahlen sind");
        for (int i = 0; i < draw.getResult().length; i++) {
            System.out.print(draw.getResult()[i] + "\t");
        }

        System.out.println("Die Superzahl ist " + draw.getSuper() + ".");

        System.out.println("Du hast damit folgende Gewinnklasse erreicht: " + ticket.eval(draw));
    }

}