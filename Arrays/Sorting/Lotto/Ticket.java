import java.util.*; 
class Ticket {
    int numbers[];
    int s;

    public Ticket(int input[], int superzahl) {
        numbers = input;
        s = superzahl;
        Arrays.sort(numbers); 
    }

    public int eval(Draw draw) {

        int correct = 0;
        for (int i = 0; i < draw.getResult().length; i++) {
            if (Draw.isElement(numbers, draw.getResult()[i]))
                correct++;
        }
        int wc = 0;
        switch (correct) {
        case 2:
            wc = 10;
            break;
        case 3:
            wc = 8;
            break;
        case 4:
            wc = 6;
            break;
        case 5:
            wc = 4;
            break;
        case 6:
            wc = 2;
            break;
        }
        if (draw.getSuper() == s)
            wc -= 1;
        if (wc < 10 && wc > 0) {
            return wc;
        }
        return 0;
    }
}