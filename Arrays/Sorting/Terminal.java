import java.util.Scanner; 
public class Terminal {
    
    public static void main(String args[]){
        Scanner t = new Scanner(System.in); 
        pl("Hallo!"); 
        p("Gib die L�nge Deines Arrays ein: "); 
        Feld a = new Feld(t.nextInt()); 
        pl("Das Feld wird nun mit zuf�lligen Zahlen gef�llt. "); 
        a.fillRandom(); 
        pl("Das Neue Feld beginnt mit folgenden zehn Inhalten: "); 
        pl(a.toString()); 
        pl("Die gr��te Zahl darin ist " + a.calcMax()); 
        pl("Die kleinste zahl darin ist " + a.calcMin()); 
        pl("Die Summe aller Zahlen betr�gt " + a.calcSum()); 
        pl("Der Mittelwert aller Zahlen betr�gt " + a.calcAverage()); 
        pl("Ich sortiere nun mit folgender Methode: BubbleSort"); 
        a.bubbleSort(); 
        pl("Das Array sieht nun folgenderma�en aus: "); 
        pl(a.toString()); 
        t.close();
        
    }

    static void p(String s){
        System.out.print(s);
    }
    static void pl(String s){
        System.out.println(s);
    }
}