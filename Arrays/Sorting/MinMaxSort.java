class MinMaxSort {
    public static void main(String args[]) {
        int arrayLength = 10;
        int array[] = new int[10];  //new int[arrayLength];
        
        long startTime = System.currentTimeMillis();
        int low = 0; 
        int high = arrayLength -1; 

         for (int i = 0; i < arrayLength; i++) {
             array[i] = (int) (Math.random() * arrayLength) + 1;
         }

            /*
        for (int i = 0; i < arrayLength; i++) {
            array[i] = (int) (Math.random()*arrayLength)+1; 
        }
        */


        while (low <= high){

            int cMin = low; 
            int cMax = low; 
            
            for (int j = low; j <= high; j++){
                if (array[cMax] < array[j]){
                    cMax = j; 
                }
                if (array[cMin] > array[j]){
                    cMin = j; 
                }
                
            }

            int max = array[cMax]; 
            int min = array[cMin]; 

            array[cMax] = array[high]; 
            array[cMin] = array[low]; 

            array[low] = min;
            array[high] = max; 
            /*
            int buffer = array[low]; 
            array[low] = array[cMin]; 
            array[cMin] = buffer; 
            buffer = array[high]; 

            if (cMin == high){
                cMin = cMax; 
                System.out.println("ERROR");
            }
            
            array[high] = array[cMax]; 
            array[cMax] = buffer; 
            */
            low++; 
            high--; 
            printArray(array, arrayLength); 
        }

        
        

        long endTime = System.currentTimeMillis();

        System.out.println("------------");

        System.out.println((endTime - startTime) / 1000f);

    }

    public static void printArray(int array[], int length){
        for (int i=0; i<length; i++){
            System.out.print(array[i] + "\t"); 
        }
        System.out.println(); 
    }

    }

