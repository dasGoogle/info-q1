class MaxSort {
public static void main(String args[]){
    int arrayLength = 1000000; 
    int array[] = new int[arrayLength]; 
    long startTime = System.currentTimeMillis(); 

    for (int i=0; i<arrayLength; i++){
        array[i]=(int) (Math.random()*arrayLength)+1; 
    }

    
    int currentMax = 0; 
    for (int i = arrayLength-2; i >= 0; i--){
        for (int j = 0; j <= i; j++){
            if (array[currentMax] < array[j]){
                currentMax = j; 
            }
        }
        int buffer = array[i+1]; 
        array[i+1] = array[currentMax]; 
        array[currentMax] = buffer; 
    }

    long endTime = System.currentTimeMillis(); 
    

    System.out.println("------------"); 

    System.out.println((endTime - startTime)/1000f); 

    
}

}
