class Feld {
    int array[];

    public Feld(int i) {
        array = new int[i];
    }

    public void fillRandom() {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (array.length * Math.random()) + 1;
        }
    }

    public int get(int i) {
        return array[i];
    }

    public int calcMin() {
        int cMin = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < cMin) {
                cMin = array[i];
            }
        }
        return cMin;
    }

    public int calcMax() {
        int cMax = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > cMax) {
                cMax = array[i];
            }
        }
        return cMax;
    }

    public long calcSum() {
        long sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    public double calcAverage() {
        return calcSum() / array.length;
    }

    public void maxSort() {
        int currentMax = 0;
        for (int i = array.length - 2; i >= 0; i--) {
            for (int j = 0; j <= i; j++) {
                if (array[currentMax] < array[j]) {
                    currentMax = j;
                }
            }
            int buffer = array[i + 1];
            array[i + 1] = array[currentMax];
            array[currentMax] = buffer;
        }
    }

    public void bubbleSort() {
        for (int i = array.length - 2; i >= 0; i--) {
            for (int j = 0; j <= i; j++) {
                if (array[j] > array[j + 1]) {
                    int buffer = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = buffer;
                }
            }
        }

    }

    public String toString(){
        String s = "["; 
        for (int i = 0; i < 10 && i<array.length; i++) {
            s += array[i] + ", ";
        }
        s += "...";
        return s; 

    }

    public int length() {
        return array.length;
    }
}