/**
  *
  * Beschreibung:  Erster Umgang mit Feldern (Arrays)
  *
  * @version 1.0 vom 16.10.2018
  * @author: J. Moch 
  */

  import java.util.*;

  public class Arrays {
    
    public static void main(String[] args) {
      
      Scanner tastatur;
      int[] zahl;
      
      tastatur = new Scanner(System.in);
      zahl = new int[5];
      
      zahl[0] = 2;
      zahl[1] = -3;
      zahl[2] = zahl[1]+4;
      zahl[3] = zahl[0] - zahl[1];
      System.out.print("Wie soll die 5. Zahl lauten? "); zahl[4] = tastatur.nextInt();
      System.out.println();
      
      System.out.println("Die 2. Zahl ist: " + zahl[1]);
      System.out.println();
      
      for (int i=0;i<=4;i++ ) {
        System.out.println("Die " + (i+1) +". Zahl lautet: " + zahl[i]);
      } // end of for

      int minimum = zahl[0]; 

      for (int i = 0; i <= 4; i++){
        if(zahl[i] < minimum){
            minimum = zahl[i]; 
        }
      }
      System.out.println("Das Minimum dieses Arrays lautet "+ minimum); 


      int maximum = zahl[0]; 

      for (int i = 0; i <= 4; i++){
        if(zahl[i] > maximum){
            maximum = zahl[i]; 
        }
      }
      System.out.println("Das Maximum dieses Arrays lautet "+ maximum); 


      int summe = 0; 

      for (int i = 0; i <= 4; i++){
        summe += zahl[i]; 
      }
      System.out.println("Die Summe dieses Arrays lautet "+ summe); 

      double mittelwert = summe / 5.0; 
      System.out.println("Der Mittelwert dieses Arrays ist "+ mittelwert);
      
      // Jetzt w�rde ich noch gern die kleinste, die gr��te, die Summe
      // und die Querschnittsumme aller Zahlen berechnet und ausgegeben haben.
      
    } // end of main
    
  } // end of class ersterUmgangMitFeldernUnvollst�ndig
  