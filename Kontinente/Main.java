/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 15.08.2018
 * @author Aaron Schlitt
 */

import java.util.Scanner; 

public class Main {
  
  public static void main(String[] args) {
    Scanner terminal = new Scanner(System.in);
    Kontinent kontinent = new Kontinent();  
    System.out.println("Bevölkerungsdichte berechnen");
    System.out.print("Namen eingeben: ");
    kontinent.setName(terminal.next());
    System.out.print("Einwohneranzahl eingeben: ");
    kontinent.setEinwohner(terminal.nextInt());
    System.out.print("Flaeche eingeben eingeben: ");
    kontinent.setFlaeche(terminal.nextInt());
    System.out.print("Die Bevölkerungsdichte ist: " + kontinent.bevoelkerungsDichte() + ".");
    terminal.close();
  } // end of main

} // end of class Main

