import com.sun.java_cup.internal.runtime.virtual_parse_stack;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 08.08.2018
 * @author 
 */

public class Kontinent {
  
  // Anfang Attribute
  private String name;
  private int einwohner;
  private int flaeche;
  // Ende Attribute
  
  public Kontinent(String name, int einwohner, int flaeche) {
    this.name = name;
    this.einwohner = einwohner;
    this.flaeche = flaeche;
  }
  
  public Kontinent(){
    
    
  }

  // Anfang Methoden
  public String getName() {
    return name;
  }

  public int getEinwohner() {
    return einwohner;
  }

  public int getFlaeche() {
    return flaeche;
  }

  public float bevoelkerungsDichte() {
    
    return Math.round((float)einwohner/(float)flaeche*100f)/100f;
  }

  public void setName(String nameNeu) {
    name = nameNeu;
  }

  public void setEinwohner(int einwohnerNeu) {
    einwohner = einwohnerNeu;
  }

  public void setFlaeche(int flaecheNeu) {
    flaeche = flaecheNeu;
  }

  // Ende Methoden
} // end of Kontinent

