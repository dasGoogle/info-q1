/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 08.08.2018
 * @author Aaron Schlitt
 */

import java.util.Scanner;

public class Hello {
  
  public static void main(String[] args) {
    Scanner terminal = new Scanner(System.in); 
    String name; 

    System.out.println("Dein Name: "); 
    name = terminal.next();

    System.out.println("Hallo " + name +", ich freue mich, dass Du Java programmierst. ");
    terminal.close(); 
  } // end of main

} // end of class Hello
